//no 1 
function range (startNum, finishNum){
    var ar = [];
    if(startNum == null || finishNum == null){
        return "-1";
    }
    else{
        if (startNum < finishNum){
            for (var i = startNum; i <= finishNum; i++){
                ar.push(i);
            }
        }else{
            for (var i =startNum; i >= finishNum; i--){
                ar.push(i);
            }
        }
        return ar;
    }
}
console.log (range(1,10))
console.log(range(1))
console.log(range(11,18))
console.log(range(54,50))
console.log(range())

//no 2 
function rangeWithStep (startNum, finishNum, step){
    var ar = [];
    if (startNum == null || finishNum == null){
        return "-1";
    } else{
        if (startNum < finishNum){
            for (var i = startNum; i <= finishNum; i -= step){
                ar.push(i);
            }
        }
        return ar;
}}

console.log (rangeWithStep (1, 10, 2))
console.log (rangeWithStep (11, 23, 3))
console.log (rangeWithStep (5, 2 ,1))
console.log (rangeWithStep (29, ,2 4))

//no 3 
function sum (startNum, finishNum, step){
    var ar = 0;
    if (startNum == null && finishNum == null){
        return = 0;
    }
    else if (startNum == null){
        return finishNum;
    }
    else if (finishNum == null){
        return startNum;
    }else{
        if (step == null){
            step = 1;
        }
        if (startNum < finishNum){
            for (var i = startNum; i <= finishNum; i += step){
                ar += i;
            }
        } else{
            for (var i = startNum; i>= finishNum; i -= step){
                ar += i;
            }
        }
        return ar;
    }
}
console.log (sum(1, 10))
console.log (sum(5, 50, 2))
console.log (sum(15,10))
console.log (sum(20, 10, 2))
console.log (sum(1))
console.log (sum())

//no 4 
function dataHandling (data){
    for (var i = 0; i < data.length; i++){
        console.log ("Nomor ID: " + data [i] [0]);
        console.log ("Nama Lengkap: " + data[i] [1]);
        console.log ("TTL: " + data [i] [2] + " " + data [i] [3]);
        console.log ("Hobi: " + data [i] [4] + "\n");
    }
}
var input = [ 
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]

    dataHandling(input);
]

// no 5 
function balikKata(data){
    var hasil = " ";
    for (var i = data.length-1; i >= 0; i--){
        hasil += data [i];
    }
    return hasil;
}
console.log (balikKata ("Kasur Rusak"))
console.log (balikKata ("Informatika"))
console.log (balikKata ("Haji Ijah"))
console.log (balikKata ("reacecar"))
console.log (balikKata ("I am Humanikers"))

//no 6 
function dataHandling (data){
    data.splice (1, 1, data[1] + "Elsharawy");
    data.splice (2, 1, "Provinsi " + data[2]);
    data.splice (4, 1, "Pria", "SMA International Metro");
    console.log (data);

    var ttl = String (data.slice(3,4));
    var bulan = ttl.split ("/");
    switch (bulan[1]){
        case '01': { var bulanTeks = "Januari"; break; }
        case '02': { var bulanTeks = "Februari"; break; }
        case '03': { var bulanTeks = "Maret"; break; }
        case '04': { var bulanTeks = "April"; break; }
        case '05': { var bulanTeks = "Mei"; break; }
        case '06': { var bulanTeks = "Juni"; break; }
        case '07': { var bulanTeks = "Juli"; break; }
        case '08': { var bulanTeks = "Agustus"; break; }
        case '09': { var bulanTeks = "September"; break; }
        case '10': { var bulanTeks = "Oktober"; break; }
        case '11': { var bulanTeks = "November"; break; }
        case '12': { var bulanTeks = "Desember"; break; }
    }
    console.log (bulanTeks);
    console.log (bulan.sort (function (value1, value2) {return value2-value1}));

    var bulan = ttl.split("/");
    console.log(bulan.join ("-"));

    var nama = String (data.slice(1,2));
    console.log (nama.slice (0,14));
}

var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);