//no 1 
//release 0
class Animal{
    constructor(name){
        this.name = 'Shaun';
        this.leg = 4;
        this.cold_blooded = 'false';
    }
    getaname(){
        return this.name;
    }
}
var sheep = new Animal ("Shaun");
console.log (sheep.name);
console.log (sheep.legs);
console.log (sheep.cold_blooded);

// release 1
//class Ape
class Ape extends Animal{
    constructor(name, yell){
        this.name = 'Kera sakti';
        this.yell = 'Auooo';
    }
    getnew(){
        return this.yell;
    }
}
var sungokong = new Ape ("Kera sakti");
console.log(sungokong.yell);

//class frog 
class Frog extends Animal{
    constructor (name,jump){
        this.name = 'buduk';
        this.jump = 'hop hop';
    }
    getnew(){
        return this.jump;
    }
}
var kodok = new Frog ("buduk");
console.log (kodok.jump);