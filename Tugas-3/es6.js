//no 1 
goldenFunction = () => {
    console.log ("this is golden");
}
goldenFunction();

//no 2 
const fullName = 'William Imoh';
console.log(fullname);

//no 3 
let newObject ={
    firstName: 'Harry',
    lastName: 'Potter Holt',
    destination: 'Hogwarts react Conf',
    occupation: 'Deve-Wizard Avocado'
};
const {firstName, lastName, destination, occupation} = newObject;
console.log(firstName, lastName, destination, occupation);

//no 4
let combined = ['Will', 'Chris', 'Sam', 'Holly', 'Gill', 'Brian', 'Noel', 'Maggie']
console.log(combined);

//no 5 
const planet = "earth";
const view = "glass";
const before = 'Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do euismod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim veniam';
console.log (before)
