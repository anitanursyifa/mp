// no 1 
var word = 'JavaScript';
var second = 'is';
var third = 'awesome';
var fourth = 'and';
var fifth = 'I';
var sixth = 'love';
var seventh = 'it!';
console.log (word +"  "+ second +" "+third +" "+ fourth +" "+fifth +" "+sixth +" "+seventh + "\n")

//no 2  
var sentence = "I am going to be React Native Developer";

var exampleFirstWord = sentence [0];
var exampleSecondWord = sentence [2] + sentence[3];
var thirdWord = sentence [5] + sentence [6]+ sentence [7] + sentence[8] + sentence[9];
var fourthWord = sentence[11] + sentence[12];
var fifthWord = sentence[14] + sentence[15];
var sixthWord = sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21];
var seventhWord = sentence[23] + sentence[24] + sentence[25] + sentence[26] + sentence[27] + sentence[28];
var eightWord = sentence[30] + sentence[31] + sentence[32] + sentence[32] + sentence[33] + sentence[34] + sentence[35] + sentence[36] + sentence[37] + sentence[38];

console.log('First Word: ' + exampleFirstWord);
console.log('Second Word: ' + exampleSecondWord);
console.log('Third Word: ' + thirdWord);
console.log('Fourth Word: ' + fourthWord);
console.log('Fifth Word: ' + fifthWord);
console.log('Sixth Word ' + sixthWord);
console.log('Seventh Word: ' + seventhWord);
console.log('Eighth Word: ' + eighthWord + '\n');

//no 3 
var sentence = "Wow Javascript is so cool";

var exampleFirstWord = sentence.substring(0,3);
var secondWord = sentence.substring(4,14);
var thirdWord = sentence.substring(15,17);
var fourthWord = sentence.substring(18,20);
var fifthWord = sentence.substring(21,25);

console.log('First Word: ' +exampleFirstWord);
console.log('Second word: ' +secondWord);
console.log('Third word: ' +thirdWord);
console.log('Fourth word: ' +fourthWord);
console.log('Fifth Word: ' +fifthWord);

//no 4 
var sentence3 = 'Wow Javascript is so Cool';

var exampleFirstWord3 = sentence3.substring(0,3);
var secondWord3 = sentence3.substring(4,14);
var thirdWord3 = sentence3.substring(15,17);
var fourthWord3 = sentence3.substring(18,20);
var fifthWord3 = sentence3.substring(21,25);

var firstWordLength = exampleFirstWord3.length;
var secondWordLength = secondWord3.length;
var thirdWordLength = fourthWord3.length;
var fifthWordLength = fifthWord3.length;

console.log ('First Word: ' +exampleFirstWord3 + ', with length: ' +firstWordLength);
console.log('Second word: ' + secondWord3 + ', with length: '+secondWordLength);
console.log('Third word: ' + thirdWord3 + ', with length: '+thirdWordLength);
console.log('Fourth word: ' + fourthWord3 + ', with length: '+fourthWordLength);
console.log('Fifth word: ' + fifthWord3 + ', with length: '+fifthWordLength);

//



